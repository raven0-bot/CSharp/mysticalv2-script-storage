local Material = loadstring(game:HttpGet("https://raw.githubusercontent.com/Kinlei/MaterialLua/master/Module.lua"))()

local UI = Material.Load({
     Title = "MysticalUIV2 - Legend Clickers",
     Style = 1,
     SizeX = 250,
     SizeY = 200,
     Theme = "Dark"
})


local WeaponPage = UI.New({
	Title = "Main"
})

WeaponPage.Toggle({
	Text = "Autofarm",
	Callback = function(value)
		if value == true then
			getgenv().FarmingLegend = true
	else
		if value == false then
			getgenv().FarmingLegend = false
		end
	end
end,
	Enabled = false
})

while wait() do
	if getgenv().FarmingLegend == true then
		game:GetService('ReplicatedStorage').Remotes.Tap:FireServer('Click')
	end
end