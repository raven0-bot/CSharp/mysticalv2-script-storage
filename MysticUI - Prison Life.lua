-- Author ArtieFuzzz#8298 

if getgenv().MysticalUI == true then
	error("MysticalUI already has been loaded", 0)
	return
end

pcall(function() getgenv().MysticalUI = true end)

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local OpenButton = Instance.new("TextButton")
local UICorner = Instance.new("UICorner")
local MainFrame = Instance.new("Frame")
local GetAK47Button = Instance.new("TextButton")
local UICorner_2 = Instance.new("UICorner")
local GetShotgunButton = Instance.new("TextButton")
local UICorner_3 = Instance.new("UICorner")
local GetM9Button = Instance.new("TextButton")
local UICorner_4 = Instance.new("UICorner")
local NeutralTeamButton = Instance.new("TextButton")
local UICorner_5 = Instance.new("UICorner")
local InmateTeamButton = Instance.new("TextButton")
local UICorner_6 = Instance.new("UICorner")
local GuardTeamButton = Instance.new("TextButton")
local UICorner_7 = Instance.new("UICorner")
local GetWeaponsButton = Instance.new("TextButton")
local UICorner_8 = Instance.new("UICorner")
local TopBarFrame = Instance.new("Frame")
local BarLabel = Instance.new("TextLabel")
local HideButton = Instance.new("TextButton")
local UICorner_9 = Instance.new("UICorner")
local UICorner_10 = Instance.new("UICorner")
local UICorner_11 = Instance.new("UICorner")

ScreenGui.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(55, 30, 45)
Frame.BorderColor3 = Color3.fromRGB(0, 0, 0)
Frame.Position = UDim2.new(0.0105092963, 0, 0.514866948, 0)
Frame.Size = UDim2.new(0, 90, 0, 34)

OpenButton.Name = "OpenButton"
OpenButton.Parent = Frame
OpenButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
OpenButton.BorderColor3 = Color3.fromRGB(66, 36, 45)
OpenButton.Position = UDim2.new(0.100241542, 0, 0.222972974, 0)
OpenButton.Size = UDim2.new(0, 72, 0, 20)
OpenButton.Font = Enum.Font.SourceSans
OpenButton.Text = "Open"
OpenButton.TextColor3 = Color3.fromRGB(255, 255, 255)
OpenButton.TextSize = 14.000

UICorner.Parent = Frame

MainFrame.Name = "MainFrame"
MainFrame.Parent = ScreenGui
MainFrame.BackgroundColor3 = Color3.fromRGB(55, 30, 45)
MainFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
MainFrame.Position = UDim2.new(0.0488444716, 0, 0.582159638, 0)
MainFrame.Size = UDim2.new(0, 384, 0, 218)
MainFrame.Active = true
MainFrame.Draggable = true

GetAK47Button.Name = "GetAK47Button"
GetAK47Button.Parent = MainFrame
GetAK47Button.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
GetAK47Button.BorderColor3 = Color3.fromRGB(0, 0, 0)
GetAK47Button.Position = UDim2.new(0.30468756, 0, 0.174311906, 0)
GetAK47Button.Size = UDim2.new(0, 96, 0, 26)
GetAK47Button.Font = Enum.Font.SourceSans
GetAK47Button.Text = "Get AK-47"
GetAK47Button.TextColor3 = Color3.fromRGB(255, 255, 255)
GetAK47Button.TextSize = 14.000

UICorner_2.CornerRadius = UDim.new(0.25, 0)
UICorner_2.Parent = GetAK47Button

GetShotgunButton.Name = "GetShotgunButton"
GetShotgunButton.Parent = MainFrame
GetShotgunButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
GetShotgunButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
GetShotgunButton.Position = UDim2.new(0.026041666, 0, 0.477064192, 0)
GetShotgunButton.Size = UDim2.new(0, 96, 0, 26)
GetShotgunButton.Font = Enum.Font.SourceSans
GetShotgunButton.Text = "Get Remington"
GetShotgunButton.TextColor3 = Color3.fromRGB(255, 255, 255)
GetShotgunButton.TextSize = 14.000

UICorner_3.CornerRadius = UDim.new(0.25, 0)
UICorner_3.Parent = GetShotgunButton

GetM9Button.Name = "GetM9Button"
GetM9Button.Parent = MainFrame
GetM9Button.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
GetM9Button.BorderColor3 = Color3.fromRGB(0, 0, 0)
GetM9Button.Position = UDim2.new(0.026041666, 0, 0.325688064, 0)
GetM9Button.Size = UDim2.new(0, 96, 0, 26)
GetM9Button.Font = Enum.Font.SourceSans
GetM9Button.Text = "Get M9"
GetM9Button.TextColor3 = Color3.fromRGB(255, 255, 255)
GetM9Button.TextSize = 14.000

UICorner_4.CornerRadius = UDim.new(0.25, 0)
UICorner_4.Parent = GetM9Button

NeutralTeamButton.Name = "NeutralTeamButton"
NeutralTeamButton.Parent = MainFrame
NeutralTeamButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
NeutralTeamButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
NeutralTeamButton.Position = UDim2.new(0.716145813, 0, 0.477064222, 0)
NeutralTeamButton.Size = UDim2.new(0, 96, 0, 26)
NeutralTeamButton.Font = Enum.Font.SourceSans
NeutralTeamButton.Text = "Switch to Neutral"
NeutralTeamButton.TextColor3 = Color3.fromRGB(255, 255, 255)
NeutralTeamButton.TextSize = 14.000

UICorner_5.CornerRadius = UDim.new(0.25, 0)
UICorner_5.Parent = NeutralTeamButton

InmateTeamButton.Name = "InmateTeamButton"
InmateTeamButton.Parent = MainFrame
InmateTeamButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
InmateTeamButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
InmateTeamButton.Position = UDim2.new(0.716145813, 0, 0.325688034, 0)
InmateTeamButton.Size = UDim2.new(0, 96, 0, 26)
InmateTeamButton.Font = Enum.Font.SourceSans
InmateTeamButton.Text = "Switch to Innamte"
InmateTeamButton.TextColor3 = Color3.fromRGB(255, 255, 255)
InmateTeamButton.TextSize = 14.000

UICorner_6.CornerRadius = UDim.new(0.25, 0)
UICorner_6.Parent = InmateTeamButton

GuardTeamButton.Name = "GuardTeamButton"
GuardTeamButton.Parent = MainFrame
GuardTeamButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
GuardTeamButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
GuardTeamButton.Position = UDim2.new(0.716145813, 0, 0.174311921, 0)
GuardTeamButton.Size = UDim2.new(0, 96, 0, 26)
GuardTeamButton.Font = Enum.Font.SourceSans
GuardTeamButton.Text = "Switch to Guard"
GuardTeamButton.TextColor3 = Color3.fromRGB(255, 255, 255)
GuardTeamButton.TextSize = 14.000

UICorner_7.CornerRadius = UDim.new(0.25, 0)
UICorner_7.Parent = GuardTeamButton

GetWeaponsButton.Name = "GetWeaponsButton"
GetWeaponsButton.Parent = MainFrame
GetWeaponsButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
GetWeaponsButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
GetWeaponsButton.Position = UDim2.new(0.026041666, 0, 0.174311921, 0)
GetWeaponsButton.Size = UDim2.new(0, 96, 0, 26)
GetWeaponsButton.Font = Enum.Font.SourceSans
GetWeaponsButton.Text = "Get Weapons"
GetWeaponsButton.TextColor3 = Color3.fromRGB(255, 255, 255)
GetWeaponsButton.TextSize = 14.000

UICorner_8.CornerRadius = UDim.new(0.25, 0)
UICorner_8.Parent = GetWeaponsButton

TopBarFrame.Name = "TopBarFrame"
TopBarFrame.Parent = MainFrame
TopBarFrame.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
TopBarFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
TopBarFrame.Size = UDim2.new(0, 384, 0, 27)

BarLabel.Name = "BarLabel"
BarLabel.Parent = TopBarFrame
BarLabel.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
BarLabel.BorderColor3 = Color3.fromRGB(66, 36, 45)
BarLabel.Position = UDim2.new(0.026041666, 0, 0.111111112, 0)
BarLabel.Size = UDim2.new(0, 126, 0, 21)
BarLabel.Font = Enum.Font.SourceSans
BarLabel.Text = "Mystic - Prison Life"
BarLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
BarLabel.TextSize = 14.000

HideButton.Name = "HideButton"
HideButton.Parent = TopBarFrame
HideButton.BackgroundColor3 = Color3.fromRGB(66, 36, 45)
HideButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
HideButton.Position = UDim2.new(0.903645813, 0, 0.111111112, 0)
HideButton.Size = UDim2.new(0, 31, 0, 21)
HideButton.Font = Enum.Font.SourceSans
HideButton.Text = "X"
HideButton.TextColor3 = Color3.fromRGB(0, 0, 0)
HideButton.TextSize = 14.000

UICorner_9.CornerRadius = UDim.new(0.075000003, 0)
UICorner_9.Parent = HideButton

UICorner_10.CornerRadius = UDim.new(0.5, 0)
UICorner_10.Parent = TopBarFrame

UICorner_11.CornerRadius = UDim.new(0.100000001, 0)
UICorner_11.Parent = MainFrame

HideButton.MouseButton1Down:connect(function()
	MainFrame.Visible = false
	Frame.Visible = true
end)

OpenButton.MouseButton1Down:connect(function()
	MainFrame.Visible = true
	Frame.Visible = false
end)

GetWeaponsButton.MouseButton1Down:connect(function()
	local Weapon = {"AK-47"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
	local Weapon = {"M9"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
	local Weapon = {"Remington 870"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end)

GetAK47Button.MouseButton1Down:connect(function()
	local Weapon = {"AK-47"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end)

GetM9Button.MouseButton1Down:connect(function()
	local Weapon = {"M9"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end)

GetShotgunButton.MouseButton1Down:connect(function()
	local Weapon = {"Remington 870"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end)

InmateTeamButton.MouseButton1Down:connect(function()
	Workspace.Remote.TeamEvent:FireServer("Bright orange")
end)

GuardTeamButton.MouseButton1Down:connect(function()
	Workspace.Remote.TeamEvent:FireServer("Bright blue")
end)

NeutralTeamButton.MouseButton1Down:connect(function()
	Workspace.Remote.TeamEvent:FireServer("Medium stone grey")
end)
