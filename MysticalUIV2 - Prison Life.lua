local Material = loadstring(game:HttpGet("https://raw.githubusercontent.com/Kinlei/MaterialLua/master/Module.lua"))()

local UI = Material.Load({
     Title = "MysticalUI V2 - Prison Life GUI",
     Style = 3,
     SizeX = 400,
     SizeY = 250,
     Theme = "Dark"
})


local WeaponPage = UI.New({
	Title = "Main"
})


WeaponPage.Button({
	Text = "Get all Weapons",
	Callback = function()
		local Weapon = {"AK-47"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
	local Weapon = {"M9"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
	local Weapon = {"Remington 870"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end
})

WeaponPage.Button({
	Text = "Get AK-47",
	Callback = function()
		local Weapon = {"AK-47"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end
})

WeaponPage.Button({
	Text = "Get Remington",
	Callback = function()
		local Weapon = {"Remington 870"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end
})

WeaponPage.Button({
	Text = "Get M9",
	Callback = function()
		local Weapon = {"M9"}
	for i,v in pairs(Workspace.Prison_ITEMS.giver:GetChildren()) do
		if v.name == Weapon[1] then
			Workspace.Remote.ItemHandler:InvokeServer(v.ITEMPICKUP)
		end
	end
end
})


local TeamPage = UI.New({
	Title = "Team Switch"
})


TeamPage.Button({
	Text = "Inmates",
	Callback = function()
		Workspace.Remote.TeamEvent:FireServer("Bright orange")
	end
})

TeamPage.Button({
	Text = "Guards",
	Callback = function()
		Workspace.Remote.TeamEvent:FireServer("Bright blue")

	end
})

TeamPage.Button({
	Text = "Neutral",
	Callback = function()
		Workspace.Remote.TeamEvent:FireServer("Medium stone grey")
	end
})